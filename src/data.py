from scipy import integrate
import numpy as np
import pylab

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

These are Jens' solutions.
"""

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the values of dx/dt, dy/dt, dz/dt given x, y and z.
    
    Inputs:
    x - an array with the values of x, y, z
    t - the value of t
    sigma, rho, beta - args

    Outputs:
    dxdt - the values of dx/dt, dy/dt, dz/dt

    """

    return sigma*(x[1]-x[0]), x[0]*(rho-x[2]) - x[1], x[0]*x[1] - beta*x[2]

def getdata(y0, T, Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - an array with the initial conditions of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """

    t = np.arange(0., T, Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10.0, 28.0, 8/3))
    return data, t

def predict_linear(x):
    """
    Function which takes the Lorenz trajectory as input and returns 
    an array of predicted values as output using linear prediction:
    x_predicted_at_{n+1} = 2 x_n - x_{n-1}
    Cf. Tutorial 1, Exercise 2.

    Input:
    x = (x_1, ..., x_N) an array of a trajectory of the x-component of 
    the solution at time intervals of width delta_t = 0.05.
    """
    # We are not predicting the zeroth and the first value and stop with
    # the prediction of x_N even though we could predict x_{N+1}
    # Thus, the last entry is pred[N] = 2*x[N-1] + x[N-2]
    pred = np.multiply(x, 2)[1:-1] - x[:-2]
    return pred

def RMSE(xp, xa):
    """
    Function which calculates the root-mean-square error (RMSE).
    
    Input:
    xp - array of predicted values EXCLUDING x_0 and x_1 which were not predicted!
    xa - array of actual values
    """
    if len(xa) != (len(xp)+2):
        raise ValueError("In RMSE(xp,xa), length of xp must be equal to (length(xa)-2)")

    # We only sum over the actually predicted values, so we do not use x[0:1]
    myerror = np.sqrt(sum((xa[2:]-xp)**2))/len(xp)

    return myerror


def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()

if __name__ == '__main__':

    mydat, mytime = getdata([-0.587, -0.563, 16.870], 100.0, 0.05)

    mypred = predict_linear(mydat[:,0])

    myerror = RMSE(mypred, mydat[:,0])
    print("RMSE equals", myerror)

    plot3D(mydat[:,0], mydat[:,1], mydat[:,2])
    pylab.show()

    pylab.plot(mytime, mydat[:,0],    'k', label='actual')
    pylab.plot(mytime, mypred,   'r', label='linear prediction')
    pylab.legend(loc='best')
    pylab.xlabel('time')
    pylab.ylabel('x(t)')
    pylab.show()

