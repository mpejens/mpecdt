import numpy as np
import pylab

def square(x):
   """
   Function to compute the square of the input. If the input is
   a numpy array, this returns a numpy array of all of the squared values.
   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the square of all of the values
   """
   return x**2

def normal(N):
   """
   Function to return a numpy array containing N samples from
   a N(0,1) distribution.
   """
   return np.random.randn(N)

def montecarlo(f, X, N):
   """
   Function to compute the Monte Carlo estimate of the expectation
   E[f(X)], with N samples

   inputs:
   f - a Python function that applies a chosen mathematical function to
   each entry in a numpy array
   X - a Python function that takes N as input and returns
   independent individually distributed random samples from a chosen
   probability distribution
   N - the number of samples to use
   """
   fX = f(X(N))
   theta = np.sum(fX)/N
   sigmahat = 1/(N-1)*(np.sum(np.power(fX-theta, 2)))
   error_estimate1 = abs(1.0 - theta)
   ee2 = 2*np.sqrt(sigmahat/N)
   return theta, error_estimate1, ee2

if __name__ == '__main__':    
    theta, err, null = montecarlo(square, normal, 10000)
    print theta

###########   Exerrcise 1:   ######
    print "When I first ran the unmodified code I got an output of 0.984390628556."

    theta, err, null = montecarlo(square, normal, 100000)
    print theta

    print "For N = 100,000 I got a value of 0.997541338282."

    
    Nsteps = np.power(10, np.arange(5,7,.01))
    mc = 0.0*Nsteps
    mcerror = mc
    error2 = mc

    for i, N in enumerate(Nsteps):
        mc[i], mcerror[i], error2[i] = montecarlo(square, normal, N)

    
    pylab.loglog(Nsteps, mcerror, label=r"$|\hat{\theta} - \theta|$")
    pylab.loglog(Nsteps, error2, label="2nd error  $\hat{\epsilon}$")    # 2nd error epsilon-hat
    pylab.loglog(Nsteps, 1/np.sqrt(Nsteps), label=r"$\frac{1}{\sqrt{N}}$")
    pylab.legend(loc='best')
    pylab.show()

    print "The confidence interval arount theta-hat gets smaller with rate sqrt(N)."


    # Continue working here. Try plot for smaller N again, because now the errors are the same in the plot!









