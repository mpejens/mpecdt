import pylab
import numpy as np
from scipy import integrate

def Vprime_bistable(x):
    return x**3-x

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvalues - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvalues = [0]
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvalues.append(t)
    return np.array(xvals), np.array(tvalues)

def V(x):
    """
    Python to calculate the value of the potential V.
    Input:
    x  -  an array of values to evaluate V at
    Output:
    an array with entries V(x[i])
    """
    return x**4/4 - x**2/2

def rhoHat(x,sigma2):
    """
    Python to calculate the value of $\^rho$ (the density of the invariant measure).
    Input:
    x  -  an array of values to evaluate the function at
    sigma2  -  a value for $\sigma^2$
    """
    almostRhoHat= lambda x: np.exp(-2./sigma2 * V(x))
    
    # calculate the approximate value of C:
    C = 1/integrate.quad(almostRhoHat, -np.inf, np.inf)[0]

    return C*almostRhoHat(x)

def PlotShortcut(k):
    """
    Some plot commands wrapped in a function as they are needed more than once in the code.
    """
    pylab.legend(loc='best')
    pylab.title("$f(x)=x^{%.f}$"%k, fontsize=(25))
    pylab.xlabel('$N$')
    pylab.show()

def TimeAveragePlot(k=1, N = 10**4, X0 = 1., sigma=.5, dt = .001, deltat = 1. ):
    """
    Python function to produce the plot of 
    (a) the time average as a function of N and 
    (b) the error.
    Inputs:
    k  -  the function f can be specified as f(x) = x^k
    N  -  the largest value of N that we want to plot the time average for
    X0 -  the initial condition X(0)
    dt -  the time step size passed on to brownian_dynamics
    delta t  -  length of the discrete time intervals between the times t_n
    """
    f = lambda x: x**k

    #------------ Computations -------------

    T = N*deltat

    X, tvalues = brownian_dynamics(X0,Vprime_bistable,sigma,dt,T,tdump=deltat)

    # remove the entries "X(0)=1" and "t=0" as we are not interested in plotting these:
    X = X[:-1]
    tvalues = tvalues[:-1]

    # calculate time averages:
    Nvalues = np.arange(N) + 1
    TAs = np.cumsum(f(X)) / Nvalues

    # For odd values of k the expectation is zero as the pdf rhoHat is symmetric:
    if k%2==1:
        EfX = 0

    if k%2==0:
        integratethis = lambda x: x**k * rhoHat(x,sigma**2)
        EfX = integrate.quad(integratethis, -np.inf, np.inf)[0]

    error = np.abs( EfX - TAs )

    #------------ Plots --------------------

    pylab.plot(Nvalues, X, label='$X(N)$', color='Gainsboro')
    pylab.plot(Nvalues, TAs, label=r'$\frac{1}{N} \sum_{n=1}^N f(X(n))$', color='MediumBlue')
    PlotShortcut(k)

    pylab.semilogy(Nvalues, error, label=r'$\left|E[f(X)] - \frac{1}{N} \sum_{n=1}^N f(X(n))\right|$')
    PlotShortcut(k)


if __name__ == '__main__':

#-------------- Exercise 1 -----------------

#    dt = 0.1
#    sigma =0.5
#    T = 1000.0
#    x,t = brownian_dynamics(0.0,Vprime_bistable,sigma,dt,T,dt)
#    pylab.plot(t,x)

#    pylab.show()

#-------------- Exercise 2 -----------------

    print('\n\n ********** Exercise 2: ********** \n\n')

    N = 1000

    X0 = 1.
    sigma =0.5
    dt = 0.1
    T = 100.
    no_of_ts = 5
    tdump = np.floor(T/no_of_ts)
    num_bins = 50

    #------------ Computations -------------

    X = np.zeros((no_of_ts+1,N))

    for n in range(N):
        X[:,n], tvalues = brownian_dynamics(X0,Vprime_bistable,sigma,dt,T,tdump)

    # remove the entries "X(0)=1" and "t=0" as we are not interested in plotting these:
    X = X[1:,:]
    tvalues = tvalues[1:]

    #------------ Plot ---------------------

    fig = pylab.figure()
    pylab.title("Histograms for %.f realisations of $X(t)$\nfor different values of $t$\n"%N, 
                fontsize=(25))
    pylab.axis('off')

    x = np.linspace(np.amin(X), np.amax(X), num_bins)

    for i,t in enumerate(tvalues):
        ax = fig.add_subplot(1, len(tvalues), i+1)
        ax.set_title("$t=$%.f" % t )
        ax.hist(X[i,:], bins=num_bins, normed=1, facecolor='PaleGreen')
        pylab.xlabel('x')
        ax.plot(x,rhoHat(x,sigma**2), 'r--', linewidth=2, \
                label=r'invariant solution $\hat\rho$')
        pylab.legend(loc='best')
        pylab.ylim([0,1])

    pylab.show()

    print('\n\nThe convergence of our Monte Carlo approximation to the steady state solution ' \
           +'is fast compared to rates of convergence which we observed in previous ' \
           +'exercises. In my first plot of the histograms for t in {250, 500, 750, 1000} ' \
           +'the histograms were already not distinguishable from each other and all very close ' \
           +'to the invariant solution.\n\n')

#-------------- Exercise 3 -----------------

    print('\n\n ********** Exercise 3: ********** \n\n')

    print('\n\nWe are about to see plots of the time average as a function of N and plots of ' \
           +'the error (i.e. the absolute value of the difference between the time average and the ' \
           +'expectation which we try to approximate). \nI recommend to display all plots ' \
           +'full screen. The plot of the time average also includes a plot of the values X(N). ' \
           +'It is interesting to zoom into the plots of the time average, especially ' \
           +'for N<5000: we can see how periods during which X(N) is in one ' \
           +'of the two potential wells influence the time average before it stabilises as N gets ' \
           +'larger.\n\n' \
           +'The plots of the error are shown on a semi-log plot. Depending on our luck with the ' \
           +'random number generator we observe a more or less clear linear downwards trend, which ' \
           +'suggests an exponential rate of convergence of the error to zero. ' \
           +'Most of the times, the linear trend is much clearer when considering f(x)=x**2.\n\n')

    TimeAveragePlot()

    TimeAveragePlot(k=2)




