import numpy as np
import pylab

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""

def quadrature(f, b, c):
    """
    Function to return the quadrature approximation of 
    \int f(x) \pi(x) dx
    for some PDF \pi(x), with weights b and quadrature points c.
    
    Inputs:
    f - the function to integrate, which takes x as input and returns f(x) 
        as output
    b - numpy array of the quadrature weights
    c - numpy array of the quadrature points

    Outputs:
    I - the value of the integral
    """

    I = 0.
    for i,b_i in enumerate(b):
        I += f(c[i])*b_i
    return I

def Unit(x):
    return 1.0

def hoch1(x):
    return x

def hoch2(x):
    return x**2

def hoch3(x):
    return x**3

def hoch4(x):
    return x**4

def Exercise2(x):
    return 1/(np.sqrt(2) * np.exp(-x**2/2)/np.exp(-(x-1)**2/(2*2)))

def Exercise3_g(x):
    return np.sqrt(2)*x+1

def Exercise3_1(x):
    return hoch1(Exercise3_g(x))

def Exercise3_2(x):
    return hoch2(Exercise3_g(x))

def Exercise3_3(x):
    return hoch3(Exercise3_g(x))

def Exercise3_4(x):
    return hoch4(Exercise3_g(x))

GaussLegendre1_b = [0.5,0.5]
GaussLegendre1_c = [0.5*(1-3.0**(-0.5)),0.5*(1+3.0**(-0.5))]

GaussHermite_b = [0.5,0.5]
GaussHermite_c = [-1,1]

# New c_i for Exercise 3. (The b_i do change in general, but here they do not.)
Ex3_GaussHermite_c = [np.sqrt(2)*(-1)+1,np.sqrt(2)+1]

if __name__ == '__main__':    
    print "Integral is "+str(quadrature(Unit, GaussLegendre1_b, GaussLegendre1_c))

    print "\nExercise 1:"
    print "\int x^2 dx is "+str(quadrature(hoch2, GaussHermite_b, GaussHermite_c))
    print "\int x^1 dx is "+str(quadrature(hoch1, GaussHermite_b, GaussHermite_c))

    print "\nExercise 2 is "+str(quadrature(Exercise2, GaussHermite_b, GaussHermite_c))

    print "\nExercise 3 replacing g:"
    print "\int x^4 is "+str(quadrature(Exercise3_4, GaussHermite_b, GaussHermite_c))
    print "\int x^3 is "+str(quadrature(Exercise3_3, GaussHermite_b, GaussHermite_c))
    print "\int x^2 is "+str(quadrature(Exercise3_2, GaussHermite_b, GaussHermite_c))
    print "\int x^1 is "+str(quadrature(Exercise3_1, GaussHermite_b, GaussHermite_c))

    print "\nExercise 3 replacing c_i:"
    print "\int x^4 is "+str(quadrature(hoch4, GaussHermite_b, Ex3_GaussHermite_c))
    print "\int x^3 is "+str(quadrature(hoch3, GaussHermite_b, Ex3_GaussHermite_c))
    print "\int x^2 is "+str(quadrature(hoch2, GaussHermite_b, Ex3_GaussHermite_c))
    print "\int x^1 is "+str(quadrature(hoch1, GaussHermite_b, Ex3_GaussHermite_c))
    print "Note that E(X^2) = Var(X) + (EX)^2 = 2+1 = 3,\ni.e., the above 3 is correct."

