import numpy as np
import pylab


####### Jens inserts code for Lab 2 Ex 1:
####### copied and pasted code from data.py; removed comments and more.

from scipy import integrate
import numpy as np
import pylab

def vectorfield(x, t, sigma, rho, beta):
    """
    see data.py for info
    """

    return sigma*(x[1]-x[0]), x[0]*(rho-x[2]) - x[1], x[0]*x[1] - beta*x[2]

def getdata(y0, T, Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - an array with the initial conditions of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """

    t = np.arange(0., T, Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10.0, 28.0, 8/3))
    return data, t

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()

if __name__ == '__main__':
    mydat, mytime = getdata([-0.587, -0.563, 16.870], 100.0, 0.05)
    myperturbeddat, sametime = getdata([-0.587*0.99, -0.563*0.99, 16.870*0.99], 100.0, 0.05)
    ## a plot of the pertubed trajectory:
    # plot3D(myperturbeddat[:,0], myperturbeddat[:,1], myperturbeddat[:,2])
    # pylab.show()

    # calculate error:
    difference = np.abs(mydat - myperturbeddat) 

    # array of three arrays of differences; size = (2000, 3)
    myerror = (np.sum(difference**2, 1))**0.5
    # np.sum(d**2,1) is equivalent to d[:,0]**2 + d[:,1]**2 + d[:,2]**2

    # Now the log plot:
    pylab.plot(mytime, np.log(myerror), 'k', label='errors at times t')
    #pylab.plot(mytime, mypred,   'r', label='linear prediction')
    # testing semilogy:
    pylab.semilogy(mytime, myerror, 'r', label='errors at times t using semilogy')

    pylab.legend(loc='best')
    pylab.xlabel('time t')
    pylab.ylabel('1/2 * ln (|x(t)-xp(t)|^2 + |y(t)-yp(t)|^2 + |x(t)-yp(t)|)^2') 

    pylab.show()


######## End of inserted (and then edited) code.

# Now the (modified) code for Exercise 3:

def linear_lorenz_vectorfield(y,t,sigma,rho,beta):
    """
    Function to integrate the Lorenz equations together
    with the linearised equations about that trajectory.
    
    inputs:
    y - array containing (x,y,z,dx,dy,dz)
    t - time, not used but needed for the interface
    sigma, rho, beta - parameters
    """

    x = y[:3]
    dx = y[3:]

    #This needs changing
    ## DONE
    xdot = np.array([sigma*(x[1]-x[0]), x[0]*(rho-x[2]) - x[1], x[0]*x[1] - beta*x[2]])
    dxdot = np.array([sigma*(dx[1]-dx[0]), dx[0]*(rho-x[2]) - dx[1] - x[0]*dx[2], x[1]*dx[0] + x[0]*dx[1] -beta*dx[2]])

    #combine xdot and dxdot into one field
    return np.concatenate((xdot,dxdot))

def Phi(x,dx,T):
    """
    Function to integrate the linearised Lorenz equations about
    a given trajectory.
    Inputs:
    x - the initial conditions for the Lorenz trajectory
    dx - the initial conditions for the perturbation
    T - the length of time to integrate for

    Outputs:
    dx - the final value of the perturbation
    """

    y0 = np.concatenate((x,dx))

    #t = np.array([0.,T])
    t = np.arange(0., T, 0.05)

    data = integrate.odeint(linear_lorenz_vectorfield, y0, t=t, args=(10.,28.,8./3.))

    return data[:,3:], t


####
# semi-ln-plot of the magnitude of the perturbation, i.e., the l_2-norm of (delta x, .., delta z):

# parameters:
T = 150
per = 0.99
# calculations:
datPhi, time = Phi([-0.587, -0.563, 16.870], [-0.587*per, -0.563*per, 16.870*per], T)

# plot, finally:
pylab.plot(time, np.log((np.sum(datPhi**2, 1))**0.5),   'k', label='ln of l2-norm of delta x vector')
pylab.legend(loc='best')
pylab.xlabel('time t')
pylab.ylabel('1/2 * ln( (delta x)^2 + (delta y)^2 + (delta z)^2)')
pylab.show()

####
# plot of "lamda", i.e., ln of l2-norm of delta x vector divided by t against time t
# starting at time t=100*0.05 as the first values are huge
pylab.plot(time[100:], np.divide( np.log((np.sum(datPhi**2, 1))**0.5)[100:], time[100:]), 'r', label='finding lambda')
pylab.legend(loc='best')
pylab.xlabel('time t')
pylab.ylabel('ln of l2-norm of delta x vector divided by t')
pylab.show()

print "\nMy estimate of lambda (using time t=" +str(T) +") is: " +str(np.log((np.sum(datPhi**2, 1))**0.5)[-1]/time[-1]) +"\n"
# Instead of just taking the last value here, I could average about the last few.. 



####
# We now test whether we obtain similar estimates for lambda if we change the choice of initial condition for the reference solution:

# input:
T2 = 150
per = 0.99
y0 = np.array([1., 2., 3.])
# calculations:
datPhi2, time2 = Phi(y0, per*y0, T2)

print "\nMy second estimate of lambda (using time t=" +str(T2) +") is: " +str(np.log((np.sum(datPhi2**2, 1))**0.5)[-1]/time2[-1]) +"\n"

# input:
T2 = 150
per = 0.99
y0 = np.array([-0.1, -0.2, 2.])
# calculations:
datPhi2, time2 = Phi(y0, per*y0, T2)

print "\nMy third estimate of lambda (using time t=" +str(T2) +") is: " +str(np.log((np.sum(datPhi2**2, 1))**0.5)[-1]/time2[-1]) +"\n"



