import numpy as np
import pylab as pl
from mpl_toolkits.mplot3d import Axes3D
from scipy import integrate
from data import vectorfield
from data import getdata
from mcmc import cumavplot


def theprior(priorsigma):
    """
    Function to draw a sample from a prior distribution given by the
    3-dimensional normal distribution with mean (5.5, 8.3, 18.7) and
    covariance matrix posteriorsigma^2*Id
    inputs: the standard deviation posteriorsigma
    outputs: the sample
    """
    return  np.array([-.5, -.55, 17.]) + priorsigma*np.random.randn(3)


def ex1part1(priorsigma):
    """
    Function to execute the code for the first part of Exercise 1.
    """
    prval = theprior(priorsigma)
    mydata, mytime = getdata(prval, 1.0, 0.05)

    fig = pl.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.plot(xs=mydata[:,0], ys=mydata[:,1], zs=mydata[:,2])
    ax.set_title(r'initial condition=('"%.2f, %.2f, %.2f" % (prval[0], prval[1], prval[2])+')')
    pl.show()


def ex1part2(priorsigma,noisesigma):
    """
    Function to execute the code for the second part of Exercise 1.
    Output: solution at time 1 plus noise aka. our observation y^hat
    """
    prval = theprior(priorsigma)
    data1 = fmfunction(prval)
    noise = noisesigma*np.random.randn(3)

    return data1+noise


def fmfunction(x):
    """
    The forward model function for Exercise 2.
    Here given by the Lorenz equations.
    input:
    x - the initial condition
    output:
    the solution at time = 1
    """
    data = integrate.odeint(vectorfield,y0=x, t=np.array([0.,1.]), args=(10.,28.,8./3.), mxstep=100000)

    return data[1,:]


def Phi(f,x,y,noisesigma):
    """
    Function to return Phi, assuming a normal distribution for 
    the observation noise, with mean 0 and variance noisesigma.
    inputs:
    f - the function implementing the forward model
    x - a value of the model state x
    y - a value of the observed data
    noisesigma - the standard deviation of the observation noise
    """
    return np.linalg.norm(f(x)-y, ord=2)**2/(2*noisesigma**2)


def mcmc2(f,prior_sampler,x0,yhat,N,beta,priorsigma,noisesigma):
    """
    Function to implement the MCMC pCN algorithm, now multivariate.
    inputs:
    f - the function implementing the forward model
    prior_sampler - a function that generates samples from the prior
    x0 - the initial condition for the Markov chain
    yhat - the observed data
    N - the number of samples in the chain
    beta - the pCN parameter

    outputs:
    xvals - the array of sample values
    avals - the array of acceptance probabilities
    """
    xvals = np.zeros([N+1,3])
    xvals[0] = x0
    avals = np.zeros(N)

    for i in range(N):
        w = prior_sampler(noisesigma)
        v = (1-beta**2)**(.5) * xvals[i] + beta*w
        avals[i] = min(1, np.exp( Phi(f,xvals[i],yhat,noisesigma) - Phi(f,v,yhat,noisesigma) ) )
        u = np.random.uniform()
        if u < avals[i]:
            xvals[i+1] = v
        else:
            xvals[i+1] = xvals[i]
        """
        # to analyse why the algorithm does not work:
        if i%100==0:
            print 'xi=', xvals[i], '  proposal v=', v
            print 'f(xi)=', f(xvals[i]), '  f(v)=', f(v),  '  yhat=', yhat
        """

    return xvals,avals


def ex2(beta,N,burnin,priorsigma,noisesigma):
    """
    Function to execute code for Exercise 2.
    input:
    beta - the pCN parameter
    burnin - the burn in time / number of samples to discard
    """
    if burnin>N:
        raise(ValueError("N has to be larger than burnin. One cannot discard more samples " \
                         "than one generates."))

    # choosing the initial condiditon:
    x0 = np.array([-0.587, -0.563, 16.870])

    # generate samples via pCN:
    xvals, avals = mcmc2(fmfunction, theprior, x0=x0, yhat=ex1part2(priorsigma,noisesigma), 
                        N=N, beta=beta, priorsigma=priorsigma, noisesigma=noisesigma)    

    cumavplot(avals)

    print '\n covarianve matrix:\n', np.cov(xvals, rowvar=0)

    meanvector = np.array([ np.mean(xvals[burnin:, 0]) , 
                            np.mean(xvals[burnin:, 1]) , 
                            np.mean(xvals[burnin:, 2])  ])

    print '\n mean vector:\n', meanvector, '\n'

    # plot the one-dimensional ''pdf'' (histogram) for each coordinate:
    for i in range(3):
        pl.hist(xvals[burnin:, i], bins=50, normed=1, facecolor='PaleGreen')
        pl.show()


if __name__ == '__main__':


#   ex1part1()
    
#    ex1part2()

    ex2(beta=.001, N=10000, burnin=1000, priorsigma=.5, noisesigma=.1)


