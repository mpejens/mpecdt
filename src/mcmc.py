import numpy as np
import pylab as pl

def lmap(x, a, b):
    """
    Function to evaluate y = a*x + b
    inputs:
    x - a numpy array of values
    a, b - parameters
    outputs:
    y - a numpy array containing y = a*x + b
    """
    return a*x + b

def myprior():
    """
    Function to draw a sample from a prior distribution given by the
    normal distribution with variance alpha.
    inputs: none
    outputs: the sample
    """
    alpha = 1.0
    return alpha**0.5*np.random.randn()

def Phi(f,x,y,sigma):
    """
    Function to return Phi, assuming a normal distribution for 
    the observation noise, with mean 0 and variance sigma.
    inputs:
    f - the function implementing the forward model
    x - a value of the model state x
    y - a value of the observed data
    sigma - the standard deviation of the noise
    """
    return (f(x)-y)**2/(2*sigma**2)

def mcmc(f,prior_sampler,x0,yhat,N,beta,sigma):
    """
    Function to implement the MCMC pCN algorithm.
    inputs:
    f - the function implementing the forward model
    prior_sampler - a function that generates samples from the prior
    x0 - the initial condition for the Markov chain
    yhat - the observed data
    N - the number of samples in the chain
    beta - the pCN parameter

    outputs:
    xvals - the array of sample values
    avals - the array of acceptance probabilities
    """
    xvals = np.zeros(N+1)
    xvals[0] = x0
    avals = np.zeros(N)

    for i in range(N):
        w = prior_sampler()
        v = (1-beta**2)**(.5) * xvals[i] + beta*w
        avals[i] = min(1, np.exp( Phi(f,xvals[i],yhat,sigma) - Phi(f,v,yhat,sigma) ) )
        u = np.random.uniform()
        if u < avals[i]:
            xvals[i+1] = v
        else:
            xvals[i+1] = xvals[i]

    return xvals,avals

def cumav(a):
    """
    Function to compute the cumulative average of a 1D array a.
    """
    return np.cumsum(a)/np.arange(1,len(a)+1)

def cumavplot(avalues):
    pl.plot(range(len(avalues)), cumav(avalues))
    pl.xlabel('$n$')
    pl.title('cumulative average $1/n \sum_{i=1}^n a(x_i,v)$')
    pl.show()

def identity(x):
    return x


if __name__ == '__main__':    
    
    print('\n\n******* TOO MANY MISTAKES IN THE QUESTION *********')
    print('********** stopped working on this  ***************\n\n')

    print('\n\n***************** Exercise 1 **********************\n\n')

    yhat = 1.
    x0 = 0.5
    beta = 0.36
    xvals,avals = mcmc(lambda x: x,myprior,x0,yhat,10000,beta, .1)

    cumavplot(avals)

    print('\n\nStarting with beta=0.5 and trying different values, ' \
            'I finally decided to use beta=' +str(beta) +'.\n\n')

    burnins = 2000

    print('theoretical mean = 0.5')
    print('sample mean = ' + str(np.mean(xvals[burnins:])) )
    print('\ntheoretical variance = ' + str(.5) )
    print('sample variance = ' + str(np.var(xvals[burnins:])) )

    pl.hist(xvals[burnins:], bins=50, normed=1, facecolor='PaleGreen')
    pl.show()

    print('\n\nYay, it does look like a normal distribution!\n\n')
    

    print('\n\n***************** Exercise 2 **********************\n\n')

    yhat = 1.
    x0 = 0.5
    xvals,avals = mcmc(lambda x: np.exp(x),myprior,x0,yhat,10000,0.34, 0.1)

    cumavplot(avals)

    # https://en.wikipedia.org/wiki/Log-normal_distribution
    lognormal_pdf = lambda x: 1./(x*np.sqrt(2*np.pi))* np.exp( - (np.log(x))**2/2 )
    x = np.linspace(.01, 2., 100 ) 
    pl.plot(x, lognormal_pdf(x))
    pl.hist(np.exp(xvals[4000:]), bins=50, normed=1, facecolor='PaleGreen')
    pl.show()

    print('\n\n***************** Exercise 3 **********************\n\n')

    yhat = 1.



