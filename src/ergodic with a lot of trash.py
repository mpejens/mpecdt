import numpy as np
import pylab
from scipy import integrate
from data import getdata
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib

from data import plot3D


def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

def ensemble(M,T):
    """
    Function to generate M random initial conditions for the Lorenz system and
    compute a solution at time T for each of them.
    Inputs:
    M - the number of initial conditions
    T - the time to integrate for
    Output:
    An array with M solutions at time T, each for one of the M random initial conditions
    """
    ens = 0.1*np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(10.0,28.,8./3.),
                                mxstep=100000)
        ens[m,:] = data[1,:]

    return ens


def somef(x):
    """
    Function to apply ergodic average to.
    inputs:
    x - array of dimension (M, 3), where 
    x[:,i] = realisation of X_i(T) in terms of "Exercise Sheet 2" (web page).
    outputs: the array [ f(x[:,1]), ..., f(x[:,M]) ], dimension (1, M)
    where f is some function (cf. question / Exercise 2);
    At the moment: f: R^3 \mapsto R, f(a, b, c) = a^2 + b^2 + c^2
    """
    
    if x.shape[1] != 3:
        raise ValueError("In somef(x), x must have shape (M,3). \
            (The second entry 3 is the bit that did not match if \
            you see this error.)")

    return x[:,0]**2 + x[:,1]**2 + x[:,2]**2

# Exercise 2:
# "Write a function to compute this spatial average, making use of the functions provided."
# This is the function:

def SAA(x, func = somef):
    """
    Function to calculate an approximation for the spacial average for Exercise 2.
    Name: Spatial average approximation (SAA).
    input:
    func -  some function f (cf. question / Exercise 2);
    if not specified otherwise, the standard choice is somef (defined before SAA).
    x -  the output of the function "ensemble", i.e.:
    x = [ [x1, y1, z1], ..., [xM, yM, zM] ],
    where each [xi, yi, zi] comes from solving the Lorenz system with
    random initial condition using the function ensemble.
    output:
    saa -  1/M \sum_{i=1}^M func(xi, yi, zi).
    
    """
    
    saa = np.average(func(x))

    return saa

# Second yellow box of Exercise 2:
# "Write a function to compute this time average at various different times."
# This is the function:

def TA(N, ic=0.1*np.random.randn(1,3), dt=1, args=(10.0, 28.0, 8./3.)):
    """
    Input:
    N  -  a value for N
    ic  -  initial condition
    dt  -  distance between the points t^n
    args  -  parameters sigma, rho, beta of the Lorenz system
    Output:
    a - the approximation to the time average
    """

    # I chose t^j = j, i.e., \delta t = 1:
    t = np.arange(0., N+1, dt)

    # Solve the Lorenz system with the above initial condition for t^0=0, t^1, ... ,t^N:
    xyz = integrate.odeint(vectorfield, ic, t=t, args=args)

    a = np.average(somef(xyz[1:]))
    # The exercise sheet says: "t^n = n\delta t, n=1,...,N";
    # thus: somef(xyz[0]) is not a summand in this average.
    
    return a

def TA_data(m=1000, ic=[-0.587, -0.563, 16.870], dt=1, args=(10.0, 28.0, 8./3.)):
    """
    Function to calculate the cumulative moving (time) average a(N) for N in {1,2,...,m},
    where m is one of the possible inputs:    
    Input:
    m  -  Largest value for N that we would like to see in the plot.
    dt  -  delta t: the fixed distance between t^n and t^{n+1}
    ic  -  initial condition
    args - the parameters sigma, rho, beta of the Lorenz system
    Output:
    t  -  array of times t^n, n=1,...,m 
    a  -  array with a[n] = "timeaverage for N=n", cf. Wikipedia link below
    """
    # specify the times t^n:
    t = np.arange(0., m+1, dt)
    # Calculate solution X at times t = 0, t^1, t^2, ...:
    data = integrate.odeint(vectorfield, ic, t=t, args=args)
    # Only take X(t^1), X(t^2), etc., not X(0) :
    x = data[1:]
    fx = somef(x)
    # Calculate the cumulative moving average of s:
    # (Cf. http://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average)
    a = 1.0*fx
    # keep a[0] = fx[0] = $f(X(1))$ and calculate a[n] for n>0:
    for n in np.arange(1.,len(a)):
        a[n] = (n/(n+1))*a[n-1] + fx[n]/(n+1)

    return t[1:], a


def getdata2(y0=[-0.587, -0.563, 16.870], T1=2000, T2=2200, Deltat=0.5, param=(10.0, 28.0, 8./3.)):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - an array with the initial conditions of the system state
    T1, T2  - times between which observations will be calculated
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    param  -  the parameters sigma, rho, beta of the Lorenz system
    """

    t = np.arange(T1, T2, Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=param)
    
    return data[1:,:], t[1:]


def plotTA(m = 1000, dt = 1, ic = [-0.587, -0.563, 16.870]):
    
    """
    Function to plot the time average in Exercise 2.
    Input:
    m   -  largest value of N that we want to plot the average for
    dt  -  \Delta t in the Exercise Sheet..
    ic  -  initial conditions

    """

    # Calculate values for a lot of N:
    print("Preparing the plot of the time average estimator as a function of N... "\
          "(This might take a while..)\n\n")
    t, a = TA_data(m=m, ic=ic, dt=dt)    

    # Plot the average as a function of N:
    plt.figure()
    plt.gca().set_position((.1, .3, .8, .6)) # to make a bit of room for extra text
    plt.figtext(.05, .09, "The approximation seems to converge exponentially fast.\n\n"\
                "The next plot will zoom in on the tail of the above graph.")

    pylab.plot(t, a, 'r', label= r'$\frac{1}{N}\sum_{n=1}^N f(x(n),y(n),z(n))$')
    pylab.legend(loc='best')
    pylab.xlabel('N')
    pylab.show()

    # Plot the tail in more detail:
    plt.figure()
    plt.gca().set_position((.1, .3, .8, .6)) # to make a bit of room for extra text
    plt.figtext(.05, .09, "A more interesting plot for different initial conditions " \
             "is about to follow.")

    pylab.plot(t[-(m/10.):], a[-(m/10.):], 'r', label= r'$\frac{1}{N}\sum_{n=1}^N f(x(n),y(n),z(n))$')
    pylab.legend(loc='best')
    pylab.xlabel('N')
    pylab.title('Zoom')
    pylab.show()


def montecarlo(T2=10, M2=10 , NOS=10):
    """
    Function to run the Monte Carlo simulation for Exercise 2.
    T2  -  T form the "exercise sheet"
    M2  -  M form the "exercise sheet"
    NOS -  number of samples (generated values) that we use for the Monte Carlo method

    """
    MC = np.zeros(NOS)

    print("The Monte Carlo simulation will now start and might take a while.\n")

    for j in range(NOS):    # will run for j from 0 to NOS-1, i.e., NOS times
        if j==np.int(NOS/10):
            print("The Monte Carlo simulation is now about 10% done.\n\n")
        x = ensemble(M2, T2)
        MC[j] = SAA(x)

    return T2, M2, NOS, MC

def exercise2part1():

    "The print commands for the first part of Exercise 2 wrapped in a function."
    "This makes it easier to comment this part out and cleans up the script.."

    print("\nOUTPUT FOR EXERCISE 2:\n")

    T2, M2, NOS, MC = montecarlo(T=10, M=10 , NOS=10)

    print("Running Monte Carlo just in this moment "\
        "gave us a sample variance of " +str(np.var(MC)) +". For this, " \
        "T = " + str(T2) + \
        ", M = " +str(M2) +", and a sample size of " +str(NOS) +" were used. \n\n" \
        "For T = 10, and M = sample size = 50, the values I came across during my " \
        "investigation lay around 255. \n\n"\
        "For T = M = sample size = 50, the values all were above 4000.\n\n" \
        "For T = M = sample size = 100, I only did one calculation (which took a while) and " \
        "gave me a sample variance of approximately 2381.\n\n" \
        "To summarise: The variance is very large.\n\n\n")



if __name__ == '__main__':

    
    ########################### Exercise 1: #######################################
    """
    # Create plots for Exercise 1:

    different_Ts = [0.5, 1., 10., 100.]
    M=100
    for iT in different_Ts:
        Ens = ensemble(M, iT)
        ensemble_plot(Ens,'T={time}'.format(time=iT))
    
    pylab.show()
    # Remark: The above computations get slow for M=100 and iT=100.
    """
    
    ########################### Exercise 2: #######################################
    """
    "Choosing a function f investigate the variance in the approximation 
     with respect to this random initial condition.
    """
#    exercise2part1()



    ########################## Second yellow box of Exercise 2:
#    print("\nSecond yellow box of Exercise 2:\n\n")




    #############################################################################
    # "Choosing a function f, plot the value of the time average as a function of N."

#    plotTA(m = 1000, dt = 1, ic = [-0.587, -0.563, 16.870])




    #############################################################################
    # What do you observe about the rate of convergence to the solution? 

    print('Question: "What do you observe about the rate of convergence to the solution?"\n'\
           'Answer: The approximation seems to converge exponentially fast.\n\n')


   




    #############################################################################
    # "Demonstrate the independence of the time average to the initial condition."
    
    # Array with different initial condidions:
    IC = [np.array([-0.587, -0.563, 16.870]),
         np.array([-0.587*0.9, -0.563*1.2, 16.870*(-0.3)]),
         0.1*np.random.randn(3)]
    # The figtext in the figure ploted below will refer to N(0,0.1^2)-randomly 
    # generated inital conditions in the red graph. Consider this when making changes.
    color = ['b', 'g', 'r']

    m = 5000
    dt = 1

    print("Preparing another plot of the time average estimator as a function of N... "\
          "(This might take a while..)\n\n")

    t = np.zeros(m)
    a = np.zeros(m)

    plt.figure()

    for i, ic in enumerate(IC):
        t, a = TA_data(m=m, ic=ic, dt=1)
        plt.plot(t, a, color[i], label= '$(x_0,y_0,z_0)=${Wert}'.format(Wert=ic))    

    plt.gca().set_position((.1, .4, .8, .55)) # to make a bit of room for extra text

    plt.figtext(.05, .09, r"$\frac{1}{N}\sum_{n=1}^N f(x(n),y(n),z(n))$ for different " \
             "initial conditions. " \
             "\nThe initial conditions for the red" + r"graph were $N(0,\ (0.1)^2)$-randomly generated." \
             "\nFor certain initial conditions the convergence seems to be slower than for others. " \
             "\nConsidering the magnitude and the spread of the individual summands in the average, "\
             "\nthe values of the averages are all close together once $N$ is large enough.")
    plt.legend(loc='best')
    plt.xlabel('N')
    plt.show()



################################   Exercise 3:   #######################################

    print "\n\nExercise 3:\n\n"

#    """
#    A first visualisation of the support of the invariant measure by using a single 
#    solution over a long time interval:
#    """
#    # Calculation of a solution for times between T1 and T2 using a modified version of
#    # the function getdata from data.py and the initial condition y0 from data.py:
#    mydat, mytime = getdata2(y0=[-0.587, -0.563, 16.870], T1=2000, T2=2200, Deltat=0.05)

#    # A 3D scatter plot:
#    from mpl_toolkits.mplot3d import Axes3D
#    fig = pylab.figure()
#    ax = Axes3D(fig)
#    ax.scatter(mydat[:,0], mydat[:,1], mydat[:,2])
#    plt.show()

#    # A 3D plot as in data.py:
#    plot3D(mydat[:,0], mydat[:,1], mydat[:,2])
#    pylab.show()

#    # A plot of the x, y, and z coordinate over time:
#    pylab.plot(mytime, mydat[:,0], 'b', label= 'x coordinate x(t)')
#    pylab.plot(mytime, mydat[:,1], 'g', label= 'y coordinate y(t)')
#    pylab.plot(mytime, mydat[:,2], 'r', label= 'z coordinate z(t)')
#    pylab.legend(loc='best')
#    pylab.xlabel('t')
#    pylab.show()


    """
     Now for various different rho,sigma,beta. 
     What happens to the shape of the attractor? Which parameter is the 
     attractor shape most sensitive to?

    """



    T1 = 2000
    T2 = 2050
    Deltat = 0.05 

    sigma = np.array([10., 5., 10., 20., 40., 50., 80.])
    rho = np.array([28., 9, 10., 20., 28., 40., 60., 80.])
    beta = np.array([8./3., 1./3., 2., 8./3., 4., 8., 10.])

    parameters = [sigma, rho, beta]


#################   TEST:   ##################
    # Here I investigated whether what I do in the for-loop-plot below generally 
    # works outside a for loop. It does.
    
    param = list((sigma[0], rho[0], beta[0]))
    param[1] = parameters[1][0]

    # Calculation of a solution for times between T1 and T2 using a modified version of
    # the function getdata from data.py and the initial condition y0 from data.py:
    mydata, mytime = getdata2(param=tuple(param), T1=T1, T2=T2, Deltat=Deltat)
    
    fig, ax = plt.subplots(2, 2)
    ax[1,0].plot(xs=mydata[:,0], ys=mydata[:,1])
    
    fig.show()

################# end of TEST:  ##############


#    NN = max(len(sigma), len(rho), len(beta))
#    
#    fig, ax = plt.subplots(3, NN)

#    for m, para in enumerate(parameters):
#        for n,p in enumerate(para):
#            param = list((sigma[0], rho[0], beta[0]))
#            param[m] = parameters[m][n]
#            mydata, mytime = getdata2(param=tuple(param), T1=T1, T2=T2, Deltat=Deltat)           

#            ax[m,n].plot(xs=mydata[:,0], ys=mydata[:,1], zs=mydata[:,2], projection='3d')
#            ax[m,n].set_title(r'$(\sigma,\rho,\beta)=${Wert}'.format(Wert=param))

#    fig.show()


#################      Which parameter is the attractor shape most sensitive to?
    print("This question was based on out standard parametrisation (sigma, rho, beta) = (10, 28, 8/3) and only one parameter at a time was changed. In this situation the attractor seems to be most sensitive to changes in beta. Th")

    #error (:






#  ##################################################################
    """
    Several old "manual" ways of plotting:   

    """

    mydat, mytime = getdata2(param=(sigma[0], rho[0], beta[0]), T1=T1, T2=T2, Deltat=Deltat)

    mydats2, mytime = getdata2(param=(sigma[1], rho[0], beta[0]), T1=T1, T2=T2, Deltat=Deltat)    
    mydats3, mytime = getdata2(param=(sigma[2], rho[0], beta[0]), T1=T1, T2=T2, Deltat=Deltat)

    mydatr2, mytime = getdata2(param=(sigma[0], rho[1], beta[0]), T1=T1, T2=T2, Deltat=Deltat)    
    mydatr3, mytime = getdata2(param=(sigma[0], rho[2], beta[0]), T1=T1, T2=T2, Deltat=Deltat)

    mydatb2, mytime = getdata2(param=(sigma[0], rho[0], beta[1]), T1=T1, T2=T2, Deltat=Deltat)    
    mydatb3, mytime = getdata2(param=(sigma[0], rho[0], beta[2]), T1=T1, T2=T2, Deltat=Deltat)


    ######################   First try: attractors for different values of sigma in one plot:    ######
    
    # 3D line plot and scatter plot:

    fig = plt.figure()

    ax = fig.add_subplot(211, projection='3d')
    ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'b', label='$\sigma =${Wert}'.format(Wert=sigma[0]))
    ax.plot(mydats2[:,0], mydats2[:,1], mydats2[:,2], 'g', label='$\sigma =${Wert}'.format(Wert=sigma[1]))
    ax.plot(mydats3[:,0], mydats3[:,1], mydats3[:,2], 'r', label='$\sigma =${Wert}'.format(Wert=sigma[2]))
    
    bx = fig.add_subplot(212, projection='3d')
    bx.scatter(mydat[:,0], mydat[:,1], mydat[:,2], color='b', label='$\sigma =${Wert}'.format(Wert=sigma[0]))
    bx.scatter(mydats2[:,0], mydats2[:,1], mydats2[:,2], color='g', label='$\sigma =${Wert}'.format(Wert=sigma[1]))
    bx.scatter(mydats3[:,0], mydats3[:,1], mydats3[:,2], color='r', label='$\sigma =${Wert}'.format(Wert=sigma[2]))
    #bx.legend(loc='upper left', numpoints=1, ncol=3, fontsize=8, bbox_to_anchor=(0, 0))

    plt.show()


#    #######################      Ultimate PLOT:     ################

    fig = plt.figure()

    ax = fig.add_subplot(331, projection='3d')
    ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'b', label='$\sigma =${Wert}'.format(Wert=sigma[0]))
    ax = fig.add_subplot(332, projection='3d')
    ax.plot(mydats2[:,0], mydats2[:,1], mydats2[:,2], 'g', label='$\sigma =${Wert}'.format(Wert=sigma[1]))
    ax = fig.add_subplot(333, projection='3d')
    ax.plot(mydats3[:,0], mydats3[:,1], mydats3[:,2], 'r', label='$\sigma =${Wert}'.format(Wert=sigma[2]))

    ax = fig.add_subplot(334, projection='3d')
    ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'b', label=r'$\rho =${Wert}'.format(Wert=rho[0]))
    ax = fig.add_subplot(335, projection='3d')
    ax.plot(mydatr2[:,0], mydatr2[:,1], mydatr2[:,2], 'g', label=r'$\rho =${Wert}'.format(Wert=rho[1]))
    ax = fig.add_subplot(336, projection='3d')
    ax.plot(mydatr3[:,0], mydatr3[:,1], mydatr3[:,2], 'r', label=r'$\rho =${Wert}'.format(Wert=rho[2]))

    ax = fig.add_subplot(337, projection='3d')
    ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'b', label=r'$\beta =${Wert}'.format(Wert=beta[0]))
    ax = fig.add_subplot(338, projection='3d')
    ax.plot(mydatb2[:,0], mydatb2[:,1], mydatb2[:,2], 'g', label=r'$\beta =${Wert}'.format(Wert=beta[1]))
    ax = fig.add_subplot(339, projection='3d')
    ax.plot(mydatb3[:,0], mydatb3[:,1], mydatb3[:,2], 'r', label=r'$\beta =${Wert}'.format(Wert=beta[2]))

    plt.show()


#    ##################     Plot of the coordinates:     ################

#    # A plot of the x, y, and z coordinate over time for different values of sigma:


#    # x coordinate:

#    pylab.subplot(3,3,1)
#    pylab.title('x coordinate')
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,0], 'b', label= '$\sigma =${Wert}'.format(Wert=sigma[0]))
#    pylab.plot(mytime, mydats2[:,0], 'g', label= '$\sigma =${Wert}'.format(Wert=sigma[1]))
#    pylab.plot(mytime, mydats3[:,0], 'r', label= '$\sigma =${Wert}'.format(Wert=sigma[2]))

#    pylab.legend(loc='best')

#    # y coordinate:

#    pylab.subplot(3,3,2)
#    pylab.title('y coordinate')
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,1], 'b')
#    pylab.plot(mytime, mydats2[:,1], 'g')
#    pylab.plot(mytime, mydats3[:,1], 'r')

#    # z coordinate:

#    pylab.subplot(3,3,3)
#    pylab.title('z coordinate')
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,2], 'b')
#    pylab.plot(mytime, mydats2[:,2], 'g')
#    pylab.plot(mytime, mydats3[:,2], 'r')


#    #################### rho = ..

#    # x coordinate:

#    pylab.subplot(3,3,4)
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,0], 'b', label= r'$\rho =${Wert}'.format(Wert=rho[0]))
#    pylab.plot(mytime, mydatr2[:,0], 'g', label= r'$\rho =${Wert}'.format(Wert=rho[1]))
#    pylab.plot(mytime, mydatr3[:,0], 'r', label= r'$\rho =${Wert}'.format(Wert=rho[2]))

#    pylab.legend(loc='best')

#    # y coordinate:

#    pylab.subplot(3,3,5)
#    pylab.xlabel('t')
# 
#    pylab.plot(mytime, mydat[:,1], 'b')
#    pylab.plot(mytime, mydatr2[:,1], 'g')
#    pylab.plot(mytime, mydatr3[:,1], 'r')

#    # z coordinate:

#    pylab.subplot(3,3,6)
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,2], 'b')
#    pylab.plot(mytime, mydatr2[:,2], 'g')
#    pylab.plot(mytime, mydatr3[:,2], 'r')


#    #################### beta = ..

#    # x coordinate:

#    pylab.subplot(3,3,7)
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,0], 'b', label= r'$\beta =${Wert}'.format(Wert=beta[0]))
#    pylab.plot(mytime, mydatb2[:,0], 'g', label= r'$\beta =${Wert}'.format(Wert=beta[1]))
#    pylab.plot(mytime, mydatb3[:,0], 'r', label= r'$\beta =${Wert}'.format(Wert=beta[2]))

#    pylab.legend(loc='best')

#    # y coordinate:

#    pylab.subplot(3,3,8)
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,1], 'b')
#    pylab.plot(mytime, mydatb2[:,1], 'g')
#    pylab.plot(mytime, mydatb3[:,1], 'r')

#    # z coordinate:

#    pylab.subplot(3,3,9)
#    pylab.xlabel('t')

#    pylab.plot(mytime, mydat[:,2], 'b')
#    pylab.plot(mytime, mydatb2[:,2], 'g')
#    pylab.plot(mytime, mydatb3[:,2], 'r')


#    pylab.show()




#######################    Exercise 3  -  2nd yellow box     ############################

    print("\n\nExercise 3, 2nd yellow box:\n\n")

    """
    Choose a test function f and plot the change in
    E_mu ( f(X) ) = \int f(x) d mu(x)
    under changes in rho around our standard value of rho=28 
    (use the ergodic property to approximate this 
    expectation from a single solution over a long time interval).
    """

  # Setup:
    rr = 0.07
    delta_rho = (2*rr)/100
    rho_range = np.arange(28.-rr, 28.+rr, delta_rho)
    N = 5000

  # Calculations:
    E = 0.0*rho_range    # array used to save the approx. values of the expectation E for different rho

  # "Approximate the partial derivative of E[f(X)] f w.r.t. rho, using a first order difference".
   # This is the array to save the values of the derivative:
    dE_drho = 0.0*E 


    print("\n\nPreparing the plot of the expectation as a function of" + r"$\rho$... "\
          "(This might take a while..)\n\n")    

    for i, rho in enumerate(rho_range):
        E[i] = TA(N=N, ic=np.array([-0.587, -0.563, 16.870]), dt=1, args=(10.0, rho, 8./3.))
        if i >=1:
            dE_drho[i] = (E[i] - E[i-1])/delta_rho

   # We did not compute the derivative for the smallest value in rho_range as we are missing
   # a value in E for this. For a nice looking plot we just do the following:
    dE_drho[0] = dE_drho[1]

    print("\nI cannot really identify a region of linear growth in rho(prime). Maybe around 28.03 as the derivative is relatively(!) smooth there. Generally the expectation could be constant in rho as the graph could be the plot of a constant around 775 plus noise. \nMy observations for the change in sigma(prime) and beta(prime) are similar.\n\n")

  # Plot:
    pylab.subplot(211)
    pylab.xlabel(r'$\rho$')
    pylab.plot(rho_range, E, 'b', label=r"$E_{\mu}[f(X)]$" + " as function of " + r"$\rho$")
    pylab.legend(loc='best')

    pylab.subplot(212)
    pylab.xlabel(r'$\rho$')
    pylab.plot(rho_range, dE_drho, 'r', label=r'$\frac{d E_{\mu}[f(X)]}{d \rho}$')
    pylab.legend(loc='best')

    plt.show()




############################
    """
    Perform similar calculations for the other two parameters. 
    
    """

    ########   For sigma:    #######

#  # Setup:
#    ss = 0.1
#    #delta_sigma = (2*ss)/200
#    #sigma_range = np.arange(10.-ss, 10.+ss, delta_sigma)
#    sigma_range = np.linspace(9.94, 20.2, 202)

#    N = 500

#  # Calculations:
#    E = 0.0*sigma_range
#    dE_dsigma = 0.0*E     

#    print("\n\nPreparing the plot of the expectation as a function of sigma... "\
#          "(This might take a while..)\n\n") 

#    for i, sigma in enumerate(sigma_range):
#        E[i] = TA(N=N, ic=np.array([-0.587, -0.563, 16.870]), dt=1, args=(sigma, 28., 8./3.))
#        if i >=2:
#            dE_dsigma[i] = (E[i] - E[i-1])/delta_sigma

#    dE_dsigma[0] = dE_dsigma[1]

#  # Plot:
#    pylab.subplot(211)
#    pylab.xlabel(r'$\sigma$')
#    pylab.plot(sigma_range, E, 'b', label=r'$E_{\mu}[f(X)]$ as function of $\sigma$')
#    pylab.legend(loc='best')

#    pylab.subplot(212)
#    pylab.xlabel(r'$\sigma$')
#    pylab.plot(sigma_range, dE_dsigma, 'r', label=r'$\frac{d E_{\mu}[f(X)]}{d \sigma}$')
#    pylab.legend(loc='best')

#    plt.show()



    ########   For beta:    #######

  # Setup:
    bb = 0.1
    delta_beta = (2*bb)/200
    beta_range = np.arange(8./3.-bb, 8./3.+bb, delta_beta)
    N = 500


  # Calculations:
    E = 0.0*beta_range
    dE_dbeta = 0.0*E

    for i, beta in enumerate(beta_range):
        E[i] = TA(N=N, ic=np.array([-0.587, -0.563, 16.870]), dt=1, args=(10., 28., beta))
        if i >=2:
            dE_dbeta[i] = (E[i] - E[i-1])/delta_beta

    dE_dbeta[0] = dE_dbeta[1]

  # Plot:
    pylab.subplot(211)
    pylab.xlabel(r'$\beta$')
    pylab.plot(beta_range, E, 'b', label=r'$E_{\mu}[f(X)]$ as function of $\beta$')
    pylab.legend(loc='best')

    pylab.subplot(212)
    pylab.xlabel(r'$\beta$')
    pylab.plot(beta_range, dE_dbeta, 'r', label=r'$\frac{d E_{\mu}[f(X)]}{d \beta}$')
    pylab.legend(loc='best')

    plt.show()





