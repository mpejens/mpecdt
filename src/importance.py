import numpy as np
import pylab

"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**2

def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.
    #J someone copied and pasted here..

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    #J someone copied and pasted again..
    """
    y = 0*x
    y[x>0] = 1.0
    y[x>1] = 0.0
    return y

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    
    output:
    theta - importance sampling estimate of E[f(X)]
    """
    y = Y(N) 
    quotient = rho(y)/rhoprime(y)
    wheights = quotient/np.sum(quotient)

    return np.sum(wheights * f(y))

def Identity(x):
    """
    Function to "compute" the identity of the input. If the input is 
    a numpy array, this returns the numpy array.
    inputs:
    x - a numpy array of values
    outputs:
    x - the same numpy array of values
    """
    return x

def ErrorInMeanPlot(Min = 2, Max = 25, base = 2, \
                    f = Identity, Y = normal, rho = uniform_pdf, rhoprime = normal_pdf, \
                    true_theta = .5):
    """
    Function to plot the error of the estimate (importance sampling estimate of the
    expectation E[f(X)]), as a function of N (the number of samples)
    
    Input:
    The plot will be produced for N in {base**m | m in {Min, Min+1, ..., Max}} .
    f, Y, rho, rhoprime - arguments passed on to the above Python funciton importance(..);
    true_theta - The true value of the parameter theta which we are trying to estimate.
    """
    Nrange = base**np.arange(Min, Max+1)
    error = 0.*Nrange
    for i,N in enumerate(Nrange):
        error[i] = np.abs(true_theta - importance(f, Y, rho, rhoprime, N) )
    pylab.loglog(Nrange, error, 'x', mew=2, basex=base, basey=base, label='error$(N)$')
    pylab.plot(Nrange, Nrange**(-.5), label=r'$\frac{1}{\sqrt{N}}$')
    pylab.xlabel('$N$')
    pylab.legend(loc='best')
    pylab.show()

def f2(x):
    """
    The function f for which we compute E[f(X)] in Exercise 2.
    """
    return np.exp(-10.*x)*np.cos(x)

def Y2_pdf(x):
    """
    Function to evaluate the PDF \pi_Y for the clustered distribution in Exercise 2. 
    If the input is a numpy array, this returns a numpy array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    z - a numpy array of \pi_Y(x)
    """
    z = 0*x
    z[x>=0] = 10.*np.exp(-10.*x)/(1.-np.exp(-10.))
    z[x>1] = 0.0
    return z

def Y2(N):
    """
    Function to return a numpy array containing N samples from 
    the clustered distribution in Exercise 2.
    
    inputs:
    N - number of samples
    
    outputs:
    y - the samples
    """
    u = np.random.uniform(size = N)
    y = -.1*np.log( 1. - u + u*np.exp(-10.) )
    return y

if __name__ == '__main__':
    theta_est = importance(square, normal, uniform_pdf, normal_pdf, 100)

    print '\nEstimated theta in Exercise 1 using N=100: ', theta_est

    print('\nIn both of the following plots we observe convergence of the error to ' +\
          r'zero with rate N**(-1/2).' +\
          'This is the rate of convergence of the Monte Carlo method that we know ' +\
          'from Dynamics Tutorial 3.\n')

    # Exercise 1:
    pylab.title('Log-log plot of the error as a function of $N$ for Exercise 1')
    ErrorInMeanPlot()

    # Exercise 2:
    true_theta2 = 10./101. - (10.*np.cos(1.) - np.sin(1.))/(101.*np.exp(10.))
    pylab.title('Log-log plot of the error as a function of $N$ for Exercise 2') 
    ErrorInMeanPlot(Min = 2, Max = 25, base = 2, \
                    f = f2, Y = Y2, rho = uniform_pdf, rhoprime = Y2_pdf, \
                    true_theta = true_theta2)



